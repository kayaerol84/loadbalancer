import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RandomLoadBalancerTest {

    private final LoadBalancer randomLoadBalancer = new RandomLoadBalancer();
    @BeforeEach
    void setUp() {
    }

    @Test
    public void registerProvider_shouldRegisterAProvider_byGivenId() {
        final String id = UUID.randomUUID().toString();
        final Provider provider = new Provider(id, "randomName", Status.ALIVE);
        randomLoadBalancer.registerProvider(provider);

        assertEquals(id, randomLoadBalancer.getAllActiveProviders().get(0).get());
    }

    @Test
    void unregisterProvider_shouldUnregisterAProvider_byGivenId() {
        final Provider provider = MockProvider.createProvider();

        randomLoadBalancer.registerProvider(provider);
        assertEquals(1, randomLoadBalancer.getAllActiveProviders().size());

        randomLoadBalancer.unregisterProvider(provider);
        assertEquals(0, randomLoadBalancer.getAllActiveProviders().size());
    }

    @Test
    void registerProviders_shouldRegister_atMost10providers() {
        final List<Provider> providers = MockProvider.createProviders();
        final String  idFor11thProvider = UUID.randomUUID().toString();
        providers.add(MockProvider.createProvider(idFor11thProvider,"11th Provider", Status.ALIVE ));
        final int providersSizeBeforeRegistry = providers.size();

        randomLoadBalancer.registerProviders(providers);
        assertEquals(providersSizeBeforeRegistry - 1, randomLoadBalancer.getAllActiveProviders().size());
    }

    @Test
    void get_shouldReturnRandomProviderId() {
        final List<Provider> providers = MockProvider.createProviders();
        randomLoadBalancer.registerProviders(providers);
        final String id = randomLoadBalancer.get();
        final Optional<Provider> matchedId = providers.stream()
                .filter(p -> p.get().equals(id))
                .findAny();
        assertTrue(matchedId.isPresent());
    }

    @Test
    void get_shouldReturnRandomProviderId_whenThereAreOnly2Providers() {
        final List<Provider> providers = new ArrayList<>();
        providers.add(MockProvider.createProvider());
        providers.add(MockProvider.createProvider());

        randomLoadBalancer.registerProviders(providers);
        final String id = randomLoadBalancer.get();
        final Optional<Provider> matchedId = providers.stream()
                .filter(p -> p.get().equals(id))
                .findAny();
        assertTrue(matchedId.isPresent());
    }

    @Test
    void get_multipleTimeCalls_shouldReturnRandomId(){
        final List<Provider> providers = MockProvider.createProviders();

    }

    @SneakyThrows
    @Test
    void heartBeatChecker_shouldExcludeDeadProviders(){
        final List<Provider> providers = MockProvider.createProviders();
        randomLoadBalancer.registerProviders(providers);

        providers.get(0).statusChanged(Status.DEAD);
        randomLoadBalancer.heartBeatChecker();
        Thread.sleep(1000L);

        final List<Provider> activeProviders = randomLoadBalancer.getAllActiveProviders();
        assertEquals(9, activeProviders.size());
    }

    @SneakyThrows
    @Test
    void heartBeatChecker_shouldReincludeActiveProviders(){
        final List<Provider> providers = MockProvider.createProviders();
        randomLoadBalancer.registerProviders(providers);

        providers.get(0).statusChanged(Status.DEAD);
        randomLoadBalancer.heartBeatChecker();
        Thread.sleep(1000L);

        final List<Provider> activeProviders = randomLoadBalancer.getAllActiveProviders();
        assertEquals(9, activeProviders.size());

        randomLoadBalancer.getAllInactiveProviders().forEach(p -> p.statusChanged(Status.ALIVE));
        Thread.sleep(3000L);
        assertEquals(10, randomLoadBalancer.getAllActiveProviders().size());
    }
}
