import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class RoundRobinLoadBalancerTest {
    private final LoadBalancer roundRobinLoadBalancer = new RoundRobinLoadBalancer();
    private final int MAX_PARALLEL_REQUEST_PER_PROVIDER = 5;

    @BeforeEach
    void setUp() {
    }

    @Test
    void get_multipleCalls_shouldReturnNextProviders() {
        final List<Provider> providers = new ArrayList<>();
        providers.add(MockProvider.createProvider("firstId", "firstProvider", Status.ALIVE));
        providers.add(MockProvider.createProvider("secondId", "secondProvider", Status.ALIVE));

        roundRobinLoadBalancer.registerProviders(providers);
        int numberOfCalls = 8;
        List<String> providerIds = IntStream.range(0, numberOfCalls)
                .parallel()
                .mapToObj(integer -> roundRobinLoadBalancer.get()).collect(Collectors.toList());

        assertEquals(numberOfCalls / 2, providerIds.stream().filter(p -> p.equals("firstId")).count());
        assertEquals(numberOfCalls / 2, providerIds.stream().filter(p -> p.equals("secondId")).count());
    }

    @Test
    void registerProvider_shouldThrowException_whenTryingToAddMoreThan10(){
        List<Provider> providers = MockProvider.createProviders();
        roundRobinLoadBalancer.registerProviders(providers);
        assertEquals(10, roundRobinLoadBalancer.getAllActiveProviders().size());

        assertThrows(ProvidersSizeExceededException.class, () ->
                roundRobinLoadBalancer.registerProvider(MockProvider.createProvider()));
    }

    @Test
    void get_shouldNotAccept_moreThanYAliveProvidersSimultaneously(){
        final List<Provider> providers = MockProvider.createProviders();
        roundRobinLoadBalancer.registerProviders(providers);

        final int maxParallelRequest = providers.size() * MAX_PARALLEL_REQUEST_PER_PROVIDER;

        IntStream.range(0, maxParallelRequest -1 )
                .parallel()
                .forEach(integer -> roundRobinLoadBalancer.get());
        // No exception thrown

        final RoundRobinLoadBalancer roundRobin  = new RoundRobinLoadBalancer();
        roundRobin.registerProviders(providers);
        assertThrows(MaxParallelRequestExceededException.class,
                () -> IntStream.range(0, maxParallelRequest )
                        .parallel()
                        .forEach(integer -> roundRobin.get()) );
    }
}
