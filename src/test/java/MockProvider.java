import java.util.*;

public class MockProvider {

    public static Provider createProvider(){
        return Provider.builder()
                .id(UUID.randomUUID().toString())
                .name("Random Name")
                .status(Status.ALIVE)
                .build();
    }
    public static Provider createProvider(String id, String name, Status status){
        return Provider.builder()
                .id(id)
                .name(name)
                .status(status)
                .build();
    }

    public static List<Provider> createProviders(){
        final Set<Provider> providers = new HashSet<>();
        for (int i=0; i< 10; i++){
            providers.add(createProvider());
        }
        return new ArrayList<>(providers);
    }
}
