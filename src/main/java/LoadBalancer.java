import lombok.NoArgsConstructor;

import java.util.*;
import java.util.concurrent.CountDownLatch;

@NoArgsConstructor
public abstract class LoadBalancer {
    private final int MAX_PROVIDERS_COUNT = 10;
    private final int MAX_PARALLEL_REQUEST = 5;
    private CountDownLatch countDownLatch;

    public Map<String, Provider> activeProvidersInCluster = new HashMap<>();
    public Map<String, Provider> inactiveProvidersInCluster = new HashMap<>();

    private final Object MONITOR = new Object();

    abstract String get();

    List<Provider> getAllActiveProviders() {
        return new ArrayList<>(activeProvidersInCluster.values());
    }
    List<Provider> getAllInactiveProviders() {
        return new ArrayList<>(inactiveProvidersInCluster.values());
    }

    public void validateRequest() {
        countDownLatch.countDown();
        if (countDownLatch.getCount() == 0) {
            throw new MaxParallelRequestExceededException("Max");
        }
    }

    public void heartBeatChecker() {
        final Timer timer = new Timer();
        timer.schedule(excludeDeadProviders(), 0, 1000);
        timer.schedule(reincludeAliveProviders(), 0, 1000);
    }

    public void registerProviders(final List<Provider> providers) {
        final int size = Math.min(providers.size(), MAX_PROVIDERS_COUNT);
        for (int i=0; i < size; i++) {
            registerProvider(providers.get(i));
        }
    }
    public void registerProvider(final Provider provider) {
        if (provider == null || provider.getStatus() == Status.DEAD) return;

        synchronized(MONITOR) {
            if (activeProvidersInCluster.size() == MAX_PROVIDERS_COUNT) {
                throw new ProvidersSizeExceededException("You can't add more providers");
            }
            inactiveProvidersInCluster.remove(provider.getId());
            activeProvidersInCluster.put(provider.get(), provider);
            System.out.println("Id: " + provider.getId() + " Name: " + provider.getName() + " Status: " + provider.getStatus() + " added to cluster");
            countDownLatch = new CountDownLatch(MAX_PARALLEL_REQUEST * getAllActiveProviders().size());
        }
    }

    public void unregisterProvider(final Provider provider) {
        if (provider == null) return;

        synchronized(MONITOR) {
            activeProvidersInCluster.remove(provider.get());
            inactiveProvidersInCluster.put(provider.getId(), provider);
            System.out.println("Id: " + provider.getId() + " Name: " + provider.getName() + " removed from cluster");
        }
    }

    public TimerTask reincludeAliveProviders() {
        return new TimerTask() {
            @Override
            public void run() {
                getAllInactiveProviders().stream().parallel()
                        .forEach(Provider::check);
                getAllInactiveProviders().stream().parallel()
                        .filter(p -> p.getHeartbeatSuccessfullyChecked() >=2)
                        .forEach(p -> registerProvider(p));
            }
        };
    }

    public TimerTask excludeDeadProviders() {
        return new TimerTask() {
            @Override
            public void run() {
                getAllActiveProviders().stream().parallel()
                        .filter(p -> !p.check())
                        .forEach(p -> unregisterProvider(p));
            }
        };
    }
}

