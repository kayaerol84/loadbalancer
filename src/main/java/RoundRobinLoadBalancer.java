import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class RoundRobinLoadBalancer extends LoadBalancer {
    private int counter = 0;
    private final ReentrantLock lock;

    public RoundRobinLoadBalancer() {
        super();
        lock = new ReentrantLock(true);
    }

    @Override
    public String get() {
        validateRequest();
        lock.lock();
        try {
            final List<Provider> providers = getAllActiveProviders();
            final Provider provider = providers.get(counter);
            counter += 1;
            if (counter == providers.size()) {
                counter = 0;
            }
            return provider.get();
        } finally {
            lock.unlock();
        }
    }
}
