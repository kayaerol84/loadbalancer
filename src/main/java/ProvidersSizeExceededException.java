public class ProvidersSizeExceededException extends RuntimeException {
    public ProvidersSizeExceededException(String errorMessage) {
        super(errorMessage);
    }
    public ProvidersSizeExceededException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
