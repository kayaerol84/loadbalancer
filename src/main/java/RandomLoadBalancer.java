import lombok.NoArgsConstructor;

import java.util.*;

@NoArgsConstructor
public class RandomLoadBalancer extends LoadBalancer {

    @Override
    String get() {
        validateRequest();
        final Random random = new Random();
        final List<Provider> providers = getAllActiveProviders();
        return providers
                .get(random.nextInt(providers.size()))
                .get();
    }
}

