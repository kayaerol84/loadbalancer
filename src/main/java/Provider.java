import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@AllArgsConstructor
public class Provider {
    private final String id;
    private final String name;
    private Status status;
    private volatile int heartbeatSuccessfullyChecked;
    public Provider(final String id, final String name, final Status status) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.heartbeatSuccessfullyChecked = 0;
    }

    public String get() {
        return id;
    }

    public void statusChanged(final Status status) {
        this.status = status;
    }

    public synchronized boolean check() {
        heartBeatChecked(this.status == Status.ALIVE);
        return this.status == Status.ALIVE;
    }

    private synchronized void heartBeatChecked(final boolean successfully) {
        this.heartbeatSuccessfullyChecked = successfully ?
                this.heartbeatSuccessfullyChecked + 1 :
                0;
    }
}
