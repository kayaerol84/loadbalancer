import lombok.SneakyThrows;

import java.util.stream.IntStream;

public class Client {
    @SneakyThrows
    public static void main(final String[] args) {
        final Client client = new Client();

        client.printNextTurn("Random");
        final LoadBalancer randomLoadBalancer = new RandomLoadBalancer();
        createProviders(randomLoadBalancer);
        client.simulateConcurrentClientRequest(randomLoadBalancer);

        client.printNextTurn("Round-Robin");
        final LoadBalancer roundRobbin = new RoundRobinLoadBalancer();
        createProviders(roundRobbin);
        client.simulateConcurrentClientRequest(roundRobbin);

        randomLoadBalancer.getAllActiveProviders().stream().skip(6).forEach(p -> p.statusChanged(Status.DEAD));

        randomLoadBalancer.heartBeatChecker();
        Thread.sleep(2000L);

        randomLoadBalancer.getAllInactiveProviders().stream().skip(2).forEach(p -> p.statusChanged(Status.ALIVE));
        Thread.sleep(2000L);

        randomLoadBalancer.getAllInactiveProviders().forEach(p -> p.statusChanged(Status.ALIVE));
    }

    private void simulateConcurrentClientRequest(final LoadBalancer loadBalancer) {
        IntStream.range(0, 20)
                .parallel()
                .forEach(i -> System.out.println("Id: " + loadBalancer.get()
                                        + " --- Request from Client: " + i
                                        + " --- [Thread: " + Thread.currentThread().getName() + "]")
                );
    }

    private void printNextTurn(final String name) {
        System.out.println("---");
        System.out.println("Clients starts to send requests to " + name + " Load Balancer");
        System.out.println("---");
    }

    public static Provider createProvider(final String id, final Status status, final int index){
        return Provider.builder()
                .id(id)
                .name("random name"+index)
                .status(status)
                .build();
    }

    public static void createProviders(LoadBalancer loadBalancer){
        for (int i=0; i< 10; i++){
            loadBalancer.registerProvider(createProvider("ProviderId_"+i, Status.ALIVE, i));
        }
    }
}
