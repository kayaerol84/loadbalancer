public class MaxParallelRequestExceededException extends RuntimeException {
    public MaxParallelRequestExceededException(String errorMessage) {
        super(errorMessage);
    }
}
