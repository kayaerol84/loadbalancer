# Load Balancer #

This API allows you to use 2 types of Load Balancing strategy

## How to set up ##

* Client class gives you an example of usage both strategies 
* Create an instance of one of the LoadBalancers.
    - `final LoadBalancer roundRobbin = new RoundRobinLoadBalancer();`
* Call `registerProviders()` if you want to add multiple Providers
* Call `registerProvider()` if you want to add one specific Provider
* Call `get()` on the LoadBalancer instance to get a Provider  
 

